from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from cymailbox import forms
from cymailbox import models

# Create your views here.

class NewMessage(generic.FormView):
    template_name = "cymailbox/new_message.html"
    form_class = forms.NewMessageForm

    def get_success_url(self):
        return reverse_lazy('accounts:user_profile', kwargs={'user_id': self.kwargs.get('pk')})

    def get_context_data(self, **kwargs):
        context = super(NewMessage, self).get_context_data(**kwargs)
        user = get_object_or_404(User, pk=self.kwargs.get('pk'), profile__enable_public_mailbox=True)
        context['user'] = user
        return context


    def form_valid(self, form):
        instance = form.instance
        context = self.get_context_data()
        instance.user = context.get('user')
        instance.save()
        return super(NewMessage, self).form_valid(form)



class Inbox(LoginRequiredMixin, generic.ListView):
    template_name = "cymailbox/inbox.html"
    context_object_name = "mailboxes"
    paginate_by = 10

    def get_queryset(self):
        return self.request.user.mailbox_set.all()



class DeleteMessage(LoginRequiredMixin, generic.RedirectView):
    url = reverse_lazy("mailbox:inbox")

    def get(self, request, *args, **kwargs):
        message = get_object_or_404(models.Mailbox, user=self.request.user, pk=self.kwargs.get('pk'))
        message.delete()
        return super(DeleteMessage, self).get(request, *args, **kwargs)