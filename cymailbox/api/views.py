from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from cymailbox.api import serializers
from cymailbox import models


class MarkMessageAsRead(generics.UpdateAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.MarkMessageAsReadSerializer

    def get_queryset(self):
        return models.Mailbox.objects.filter(
            user=self.request.user, pk=self.kwargs.get('pk'))


class MessageData(generics.RetrieveAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.MessageDataSerializer

    def get_queryset(self):
        return models.Mailbox.objects.filter(
            user=self.request.user, pk=self.kwargs.get('pk'))
