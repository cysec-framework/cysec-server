from django.urls import path
from cymailbox.api import views

app_name = "mailbox-api"

urlpatterns = [
    path(
        "<str:pk>/read", views.MarkMessageAsRead.as_view(),
        name="mark-message-read"),

    path(
        '<str:pk>/body', views.MessageData.as_view(),
        name='message-body'),

]