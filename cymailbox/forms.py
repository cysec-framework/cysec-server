from django import forms
from cymailbox import models


class NewMessageForm(forms.ModelForm):
    class Meta:
        model = models.Mailbox
        fields = ('subject', 'sender_email_address', 'message')
