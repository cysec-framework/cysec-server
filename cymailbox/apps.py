from django.apps import AppConfig


class CymailboxConfig(AppConfig):
    name = 'cymailbox'
