from django.urls import path
from cymailbox import views


app_name = "mailbox"

urlpatterns = [
    path(
        '<int:pk>/new', views.NewMessage.as_view(),
        name="new_message"),
    
    path(
        'message/<str:pk>/delete', views.DeleteMessage.as_view(),
        name="delete-message"),
    
    path(
        'inbox', views.Inbox.as_view(),
        name="inbox"),
]