""" disclose all bug bounty vulnerabilities
that reached deadline
"""
import os
import datetime
import logging
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cysec_server.settings")
import django
django.setup()
from bug_bounty_mode.models import BugBountyVulnerability

logging.basicConfig(level=logging.INFO)


if __name__ == '__main__':
    vulns = BugBountyVulnerability.objects.filter(
        disclosed=False,
        disclose_deadline__lte=datetime.datetime.now()
    )

    for vuln in vulns:
        vuln.disclosed = True
        vuln.disclosed_at = datetime.datetime.now()
        vuln.save()
        logging.info("disclosed %s", vuln)
