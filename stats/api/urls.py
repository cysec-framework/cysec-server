from django.urls import path
from stats.api import views

urlpatterns = [

    path(
        'bug-bounty/vulnerability-types/top', views.TopBugBountyVulnerabilities.as_view(),
        name="top_bug_bounty_vuln_types"),

    path(
        'bug-bounty/users/top', views.TopUsersByBugBountyVulnerabilities.as_view(),
        name="top_users_by_bug_bounty_vulns"),

    path(
        'bug-bounty/vulnerabilities/latest', views.LatestBugBountyVulnerabilitiesPerDay.as_view(),
        name="latest_bug_bounty_vulns_per_day"),

    path(
        'bug-bounty/domains/most-vulnerable', views.MostVulnerableDomains.as_view(),
        name="most_vulnerable_domains")
]
