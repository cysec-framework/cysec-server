from django.contrib.auth.hashers import PBKDF2PasswordHasher


class CustomPBKDF2PasswordHasher(PBKDF2PasswordHasher):
    """
    A subclass of PBKDF2PasswordHasher that uses 60 times more iterations.
    """
    iterations = PBKDF2PasswordHasher.iterations * 60