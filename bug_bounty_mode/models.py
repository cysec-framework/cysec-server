import datetime
import uuid
from django.conf import settings
from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from pinax.badges.registry import badges
from bug_bounty_mode import badges as my_badges

# Create your models here.


class BugBountyVulnerability(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, primary_key=True)
    vuln_type = models.ForeignKey('bug_bounty_mode.VulnerabilityType', on_delete=models.CASCADE)
    domain = models.ForeignKey('BugBountyDomain', on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)
    parameter = models.CharField(max_length=64)
    path = models.CharField(max_length=128)
    poc = models.TextField()
    poc_image = models.ImageField(upload_to="pocs", blank=True, null=True)
    fixed = models.BooleanField(default=False)
    fixed_at = models.DateField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    disclose_deadline = models.DateField(blank=True, null=True)
    disclosed = models.BooleanField(default=False)
    disclosed_at = models.DateField(blank=True, null=True)
    owner_contact_email = models.EmailField()
    send_to_owner = models.BooleanField(default=False)
    send_to_owner_at = models.DateTimeField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    send_email_copy_to_user = models.BooleanField(default=False)

    def __str__(self):
        return "%s on %s" % (self.vuln_type.name, self.domain.domain)

    def add_disclosure_time(self, days):
        self.disclose_deadline = self.disclose_deadline + datetime.timedelta(days=int(days))
        self.save()

    def get_absolute_url(self):
        return reverse('bug_bounty_single_vuln', kwargs={'pk': self.pk})

    class Meta:
        verbose_name = 'Vulnerability'
        verbose_name_plural = 'Vulnerabilities'
        unique_together = ('domain', 'parameter', 'path', 'vuln_type')
        ordering = ('-created_at', )


class BugBountyDomain(models.Model):
    domain = models.URLField(unique=True)
    created_at = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.domain

    class Meta:
        verbose_name = 'Domain'
        verbose_name_plural = 'Domains'


class Comment(models.Model):
    """ comment class for a bug bounty vuln.
        can be either be from a pentester or domain owner
    """
    creator = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True)
    domain_owner = models.CharField(max_length=64, blank=True, null=True)
    vulnerability = models.ForeignKey(BugBountyVulnerability, on_delete=models.CASCADE)
    message = models.TextField()

    def __str__(self):
        return self.message


class VulnerabilityType(models.Model):
    name = models.CharField(unique=True, max_length=128)
    slug = models.SlugField()
    description = models.TextField()
    how_to_fix = models.TextField()
    consequences = models.TextField()
    updated_at = models.DateTimeField(auto_now=True)
    updated_from = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify(self.name)
        return super(VulnerabilityType, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Type'
        verbose_name_plural = 'Types'


# Register Badges here
badges.register(my_badges.SecuredWebsitesBadge)
badges.register(my_badges.PatchedBadge)