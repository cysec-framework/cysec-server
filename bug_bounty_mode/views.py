""" Bug Bounty Mode views """
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.core.mail import BadHeaderError
from django.db.models import Count, Q
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.utils import timezone
from django.views import generic
from notifications.signals import notify
from pinax.badges.registry import badges
from bug_bounty_mode.utils.auth.token import validate_access_token
from bug_bounty_mode.utils.mail.sending import send_vulnerability_mail
from .app_settings import MASTODON_ENABLED
from .forms import IncreaseDisclosureTimeForm, NewCommentForm
from .forms import NewVulnerabilityForm, ResendEmailForm
from bug_bounty_mode import forms
from bug_bounty_mode import models
from .models import BugBountyVulnerability, BugBountyDomain, Comment
from .utils.social_media.mastodon_client import MastodonClient


class SubmitNewVulnerability(LoginRequiredMixin, generic.CreateView):
    template_name = 'bug_bounty_mode/submit_vulnerability.html'
    form_class = NewVulnerabilityForm

    def form_valid(self, form):
        form.instance.user = self.request.user
        domain, _created = BugBountyDomain.objects.get_or_create(
            domain=form.cleaned_data['domain_name'])
        form.instance.domain = domain
        form.instance.send_email_copy_to_user = form.cleaned_data['send_email_copy_to_user_check']
        try:
            send_vulnerability_mail(
                self.request, form.instance, form.cleaned_data['publish_vulnerability'])
            badges.possibly_award_badge("secured_websites_awarded", user=self.request.user)
            badges.possibly_award_badge("patched_websites_awarded", user=self.request.user)

            messages.add_message(
                self.request, messages.SUCCESS, 'Thank you for submitting a vulnerability!')
            if MASTODON_ENABLED:
                client = MastodonClient()
                client.publish_toot(self.request, form.instance)
        except BadHeaderError:
            messages.add_message(
                self.request, messages.ERROR,
                'Bad Mail Header, we didnt send the email. Do it on your own!')
            form.instance.delete()
        except Exception as error:
            messages.add_message(self.request, messages.ERROR, 'Could not send mail: %s' % error)
            form.instance.delete()
        return super().form_valid(form)


class ResendEmailView(LoginRequiredMixin, generic.FormView):
    """ resend email to domain owner """
    form_class = ResendEmailForm
    http_method_names = ["post"]
    success_url = "/"

    def form_valid(self, form):
        try:                
            vuln = BugBountyVulnerability.objects.get(pk=self.kwargs['pk'], user=self.request.user)
            send_vulnerability_mail(self.request, vuln, True)
            messages.add_message(
                self.request, messages.SUCCESS, 'Resend E-Mail!')
            self.success_url = reverse_lazy('bug_bounty_single_vuln', kwargs={'pk': vuln.pk})
        except BadHeaderError:
            messages.add_message(
                self.request, messages.ERROR,
                'Bad Mail Header, we didnt send the email. Do it on your own!')
        except Exception as error:
            messages.add_message(self.request, messages.ERROR, 'Could not send mail: %s' % error)
        return super(ResendEmailView, self).form_valid(form)


class SingleVulnerability(generic.DetailView):
    # TODO: Check in view and only return fields that are allowed to be seen
    # TODO: return 404 if vuln is draft
    model = BugBountyVulnerability
    context_object_name = "vuln"
    template_name = "bug_bounty_mode/single_vulnerability.html"


    def get_context_data(self, **kwargs):
        context = super(SingleVulnerability, self).get_context_data(**kwargs)
        context.update({
            'resend_email_form': ResendEmailForm(),
            'new_comment_form': NewCommentForm()
        })
        return context


class IndexView(generic.ListView):
    context_object_name = 'lastest_vulns'
    template_name = 'bug_bounty_mode/index.html'
    queryset = BugBountyVulnerability.objects.filter(send_to_owner=True).order_by('-created_at')[:10]


    def get_context_data(self, **kwargs):
        vulns = BugBountyVulnerability.objects.filter(send_to_owner=True)
        context = super(IndexView, self).get_context_data(**kwargs)
        context['latest_vulns'] = vulns.order_by('-created_at')[:10]
        context['latest_patched'] = BugBountyVulnerability.objects.filter(
            fixed=True).order_by('-fixed_at')[:10]
        context['top_researchers'] = User.objects.filter(
            bugbountyvulnerability__send_to_owner=True).distinct().annotate(
                count=Count('bugbountyvulnerability__pk')).filter(
                    count__gte=1).order_by('-count')[:10]
        return context


class ListAllVulnerability(generic.ListView):
    queryset = BugBountyVulnerability.objects.filter(send_to_owner=True).order_by('-created_at')
    template_name = 'bug_bounty_mode/list_vulnerabilities.html'
    context_object_name = "vulns"
    paginate_by = 10



class NewCommentForVulnerability(generic.FormView):
    http_method_names = ["post"]
    form_class = NewCommentForm
    success_url = reverse_lazy('bug_bounty_index')

    def form_valid(self, form):
        if self.request.user.is_authenticated:
            vuln = get_object_or_404(
                BugBountyVulnerability, user=self.request.user, pk=self.kwargs.get('pk'))
            Comment.objects.create(
                creator=self.request.user,
                message=form.cleaned_data.get('comment'),
                vulnerability=vuln)
            # TODO: Send email to domain owner to nofify that new comment exists
            if form.cleaned_data.get('is_fixed') and bool(form.cleaned_data['is_fixed']) is True:
                vuln.fixed_at = timezone.now()
                vuln.disclosed_at = timezone.now()
                vuln.disclosed = True
                vuln.fixed = True
                vuln.save()
            self.success_url = reverse_lazy('bug_bounty_single_vuln', kwargs={'pk': vuln.pk})
        elif self.request.session.get('is_domain_owner'):
            if self.request.session.get('is_domain_owner') == self.kwargs.get('pk'):
                vuln = get_object_or_404(BugBountyVulnerability, pk=self.request.session.get('is_domain_owner'))
                Comment.objects.create(
                    domain_owner=vuln.domain.domain,
                    message=form.cleaned_data.get('comment'),
                    vulnerability=vuln)
                notify.send(vuln.domain, recipient=vuln.user,
                            verb='commented on a vulnerability', action_object=vuln, public=False)
                if form.cleaned_data.get('is_fixed') and bool(form.cleaned_data['is_fixed']) is True:
                    vuln.fixed_at = timezone.now()
                    vuln.fixed = True
                    vuln.disclosed_at = timezone.now()
                    vuln.disclosed = True
                    vuln.save()
                self.success_url = reverse_lazy('bug_bounty_single_vuln', kwargs={'pk': vuln.pk})
            else:
                messages.add_message(self.request, messages.ERROR, 'Access denied!')
        else:
            messages.add_message(self.request, messages.ERROR, 'Access denied!')
        return super(NewCommentForVulnerability, self).form_valid(form)


class IncreaseDisclosureTime(LoginRequiredMixin, generic.FormView):
    form_class = IncreaseDisclosureTimeForm

    def form_valid(self, form):
        vuln = BugBountyVulnerability.objects.get(pk=self.kwargs.get('pk'), user=self.request.user)
        if not vuln.disclosed:
            vuln.add_disclosure_time(form.cleaned_data.get('days'))
            vuln.save()
            messages.add_message(
                self.request, messages.SUCCESS,
                'updated disclosure deadline to %s' % vuln.disclose_deadline)
        else:
            messages.add_message(
                self.request, messages.WARNING, 'vulnerability already disclosed!')
        self.success_url = vuln.get_absolute_url()
        return super(IncreaseDisclosureTime, self).form_valid(form)



class VulnerabilityByUser(generic.ListView):
    """ list vulnerabilities found by given user """
    template_name = "bug_bounty_mode/vulnerability_by_user.html"
    context_object_name = "vulns"
    model = User
    paginate_by = 10

    def get_queryset(self):
        queryset = BugBountyVulnerability.objects.filter(
            user__pk=self.kwargs.get('pk'), send_to_owner=True)
        return queryset


    def get_context_data(self, *args, **kwargs):
        context = super(VulnerabilityByUser, self).get_context_data(*args, **kwargs)
        context['user'] = User.objects.get(pk=self.kwargs.get('pk'))
        return context



class EditVulnerability(LoginRequiredMixin, generic.UpdateView):
    """ edit vulnerability """
    form_class = NewVulnerabilityForm
    template_name = "bug_bounty_mode/edit_vulnerability.html"

    def get_queryset(self):
        queryset = BugBountyVulnerability.objects.filter(
            pk=self.kwargs.get('pk'), user=self.request.user, disclosed=False)
        return queryset


    def get_initial(self):
        initial = super(EditVulnerability, self).get_initial()
        initial['domain_name'] = self.get_queryset().get().domain.domain
        return initial


    def get_context_data(self, **kwargs):
        context = super(EditVulnerability, self).get_context_data(**kwargs)
        context.update({
            'increase_disclosure_time_form': IncreaseDisclosureTimeForm(),
            'vuln': self.get_queryset().get()
        })
        return context


    def form_valid(self, form):
        form.instance.user = self.request.user
        domain, _created = BugBountyDomain.objects.get_or_create(
            domain=self.request.POST.get('domain_name'))
        form.instance.domain = domain
        form.instance.send_email_copy_to_user = form.cleaned_data['send_email_copy_to_user_check']
        try:
            send_vulnerability_mail(
                self.request, form.instance, form.cleaned_data['publish_vulnerability'])
            messages.add_message(
                self.request, messages.SUCCESS, 'Thank you for submitting a vulnerability!')
            if MASTODON_ENABLED:
                client = MastodonClient()
                client.publish_toot(self.request, form.instance)
        except BadHeaderError:
            messages.add_message(
                self.request, messages.ERROR,
                'Bad Mail Header, we didnt send the email. Do it on your own!')
            form.instance.delete()
        except Exception as error:
            messages.add_message(self.request, messages.ERROR, 'Could not send mail: %s' % error)
            form.instance.delete()
        form.instance.save()
        return super().form_valid(form)


class ListUsersDraftVulnerabilities(LoginRequiredMixin, generic.ListView):
    """ list vulnerabilities of logged in user, that are not published yet """
    template_name = 'bug_bounty_mode/list_users_draft_vulnerabilities.html'
    context_object_name = "vulnerabilities"

    def get_queryset(self):
        return BugBountyVulnerability.objects.filter(~Q(send_to_owner=True), user=self.request.user)


class OwnerBugDetails(generic.DetailView):
    context_object_name = "vuln"
    template_name = "bug_bounty_mode/single_vulnerability.html"

    def get_queryset(self):
        is_domain_owner, queryset = validate_access_token(self.kwargs.get('uidb64'), self.kwargs.get('token'))
        if is_domain_owner:
            if self.request.session.get('is_domain_owner'):
                # remove old session
                del self.request.session['is_domain_owner']
            tmp_vuln_sess = get_object_or_404(queryset)
            self.request.session['is_domain_owner'] = str(tmp_vuln_sess.pk)
        return queryset

    def get_context_data(self, **kwargs):
        context = super(OwnerBugDetails, self).get_context_data(**kwargs)
        context['new_comment_form'] = NewCommentForm
        return context


class CreateVulnType(LoginRequiredMixin, generic.CreateView):
    form_class = forms.CreateVulnTypeForm
    template_name = "bug_bounty_mode/create_new_vuln_type.html"
    success_url = reverse_lazy('list_vuln_types')

    def form_valid(self, form):
        instance = form.instance
        instance.updated_from = self.request.user
        return super(CreateVulnType, self).form_valid(form)


class EditVulnType(LoginRequiredMixin, generic.UpdateView):
    form_class = forms.CreateVulnTypeForm
    template_name = "bug_bounty_mode/edit_vuln_type.html"
    success_url = reverse_lazy('list_vuln_types')

    def get_queryset(self):
        return models.VulnerabilityType.objects.filter(slug=self.kwargs.get('slug'))


    def form_valid(self, form):
        instance = form.instance
        instance.updated_from = self.request.user
        return super(EditVulnType, self).form_valid(form)


class SingleVulnType(LoginRequiredMixin, generic.DetailView):
    template_name = "bug_bounty_mode/single_vuln_type.html"
    context_object_name = "vuln_type"

    def get_queryset(self):
        return models.VulnerabilityType.objects.filter(slug=self.kwargs.get('slug'))


class AllVulnTypes(LoginRequiredMixin, generic.ListView):
    """  lists all vulnerability types """
    context_object_name = "vuln_types"
    template_name = "bug_bounty_mode/list_vuln_types.html"
    queryset = models.VulnerabilityType.objects.all()
    paginate_by = 10