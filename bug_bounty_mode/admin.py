from django.contrib import admin
from .models import BugBountyVulnerability, BugBountyDomain, Comment
# Register your models here.

class BugBountyVulnerabilityAdmin(admin.ModelAdmin):
    readonly_fields = ("created_at",)
    ordering = ('-created_at',)


admin.site.register(BugBountyDomain)
admin.site.register(BugBountyVulnerability, BugBountyVulnerabilityAdmin)
admin.site.register(Comment)
