from django.apps import AppConfig


class BugBountyModeConfig(AppConfig):
    name = 'bug_bounty_mode'
