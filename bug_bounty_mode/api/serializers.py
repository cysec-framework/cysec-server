from django.contrib.auth.models import User
from rest_framework import serializers
from bug_bounty_mode.models import BugBountyDomain, BugBountyVulnerability
from bug_bounty_mode import models

class BugHunterSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username',)


class DomainSerializer(serializers.ModelSerializer):
    class Meta:
        model = BugBountyDomain
        fields = ('domain', 'pk')
        read_only_fields = ('pk', )


class VulnerabilityTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.VulnerabilityType
        fields = ('pk', 'name', 'slug', 'description', 'how_to_fix', 'consequences')
        read_only_fields = ('pk', 'slug', )


class BugSerializer(serializers.ModelSerializer):

    class Meta:
        model = BugBountyVulnerability
        fields = (
            'domain', 'vuln_type', 'path', 'parameter', 'user', 'pk',
            'poc', 'owner_contact_email',
            'send_email_copy_to_user', 'description')
        read_only_fields = ('user', 'pk', )

    def create(self, validated_data):
        validated_data['user'] = self.context.get('request').user
        return super(BugSerializer, self).create(validated_data)
