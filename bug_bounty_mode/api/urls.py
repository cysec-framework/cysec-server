from django.urls import path
from bug_bounty_mode.api import views

app_name = 'bug-bounty-api'

urlpatterns = [

    path('domains/new', views.CreateDomain.as_view(), name="create-domain"),

    path('bugs/new', views.CreateBug.as_view(), name="create-bug"),

    path('vulnerability-types/new', views.CreateVulnerabilityType.as_view(), name="create-vulnerability-type"),

    # list all bug bounty vulnerabilities
    path('bugs', views.ListMyVulnerabilities.as_view(), name="list-bugs"),
]
