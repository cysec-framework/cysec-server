""" url mapping """
import notifications.urls
from django.urls import path, include
from . import views

urlpatterns = [

    # submit a new bug bounty vulnerability
    path('submit/', views.SubmitNewVulnerability.as_view(), name='bug_bounty_submit'),

    # show homepage of bug bounty and server stats
    path('', views.IndexView.as_view(), name='bug_bounty_index'),

    # displays details of a single bug bounty vulnerability
    path(
        'vulnerabilities/<str:pk>/',
        views.SingleVulnerability.as_view(),
        name="bug_bounty_single_vuln"),

    # resend the email to domain owner
    path(
        'vulnerabilities/<str:pk>/resend_mail/',
        views.ResendEmailView.as_view(),
        name="bug_bounty_resend_email_view"),


    # adding additional time to the disclose deadline
    path(
        'vulnerabilities/details/<str:pk>/disclosure_time',
        views.IncreaseDisclosureTime.as_view(),
        name="bug_bounty_add_disclosure_time"),


    # displays a list of all found vulnerabilities
    path('vulnerabilities/', views.ListAllVulnerability.as_view(), name='bug_bounty_list_vulns'),

    path('vulnerabilities/user/<str:pk>', views.VulnerabilityByUser.as_view(), name="bug_bounty_vuln_by_user"),

    path('vulnerabilities/drafts', views.ListUsersDraftVulnerabilities.as_view(), name="bug_bounty_list_users_drafts"),

    # publishing a new comment to a given vulnerability
    path(
        'vulnerabilities/details/<str:pk>/comments/new',
        views.NewCommentForVulnerability.as_view(),
        name="bug_bounty_comment_vuln"),

    path(
        'vulnerabilities/details/<str:pk>/edit',
        views.EditVulnerability.as_view(),
        name="bug_bounty_edit_vuln"),

    path('inbox/notifications/', include(notifications.urls, namespace='notifications')),

    path('vulnerabilities/hidden/<str:pk>/<str:uidb64>/<str:token>', views.OwnerBugDetails.as_view(), name="owner-bug-details"),

    path("types/new", views.CreateVulnType.as_view(), name="create_new_vuln_type"),
    path("types/<str:slug>", views.SingleVulnType.as_view(), name="single_vuln_type"),
    path("types/<str:slug>/edit", views.EditVulnType.as_view(), name="edit_vuln_type"),
    path("types", views.AllVulnTypes.as_view(), name="list_vuln_types"),
]
