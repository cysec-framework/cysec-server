""" Social Media Client for Mastodon Network """
import os
from django.urls import reverse
from mastodon import Mastodon
from bug_bounty_mode.app_settings import MASTODON_URL, MASTODON_EMAIL
from bug_bounty_mode.app_settings import MASTODON_PASSWORD, MASTODON_TOOT_MESSAGE

class MastodonClient:

    def __init__(self):
        if not os.path.exists('pytooter_clientcred.secret'):
            # Register app - only once!
            Mastodon.create_app(
                'cysecapp',
                api_base_url=MASTODON_URL,
                to_file='pytooter_clientcred.secret'
            )

        mastodon = Mastodon(
            client_id='pytooter_clientcred.secret',
            api_base_url=MASTODON_URL
        )
        mastodon.log_in(
            MASTODON_EMAIL,
            MASTODON_PASSWORD,
            to_file='pytooter_usercred.secret'
        )

        # Create actual API instance
        self.mastodon = Mastodon(
            access_token='pytooter_usercred.secret',
            api_base_url=MASTODON_URL
        )

    def publish_toot(self, request, vuln):
        link = request.build_absolute_uri(
            reverse('bug_bounty_single_vuln', kwargs={'pk':vuln.pk}))
        message = MASTODON_TOOT_MESSAGE
        message.replace("{{VULNERABILITY}}", str(vuln))
        message = message.replace("{{LINK}}", link)
        self.mastodon.toot(message)
