""" Access Token Utils """
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_decode
from bug_bounty_mode.models import BugBountyVulnerability
from bug_bounty_mode.utils.auth.tokens import owner_access_token


def validate_access_token(uidb64, token):
    tmp_vuln = BugBountyVulnerability.objects.none()
    is_domain_owner = None
    try:
        ub64id = force_text(urlsafe_base64_decode(uidb64))
        tmp_vuln = BugBountyVulnerability.objects.filter(pk=ub64id, send_to_owner=True, disclosed=False)
        if tmp_vuln.exists() and owner_access_token.check_token(tmp_vuln.get(), token):
            is_domain_owner = True
        else:
            tmp_vuln = BugBountyVulnerability.objects.none()
    except (TypeError, ValueError, OverflowError):
        pass
    except Exception:
        pass
    return is_domain_owner, tmp_vuln
