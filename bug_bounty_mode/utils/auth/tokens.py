from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils import six

"""
class VulnerabilityAccessTokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, vulnerability, timestamp):
        return (
            six.text_type(vulnerability.pk) + six.text_type(timestamp) +
            six.text_type(vulnerability.disclosed) + six.text_type(vulnerability.disclosed_at)
        )

vuln_access_token = VulnerabilityAccessTokenGenerator()
"""

class OwnerAccessTokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, vulnerability, timestamp):
        return (
            six.text_type(vulnerability.pk) + six.text_type(vulnerability.disclosed) +
            six.text_type(vulnerability.disclosed_at)
        )

owner_access_token = OwnerAccessTokenGenerator()