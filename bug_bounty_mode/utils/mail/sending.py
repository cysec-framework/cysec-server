""" helpers for bug bounty mode app"""
import datetime
from django.core.mail import send_mail
from django.urls import reverse
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.template.loader import render_to_string
from bug_bounty_mode.app_settings import BUG_BOUNTY_DAYS_TILL_DISCLOSURE
from bug_bounty_mode.utils.auth.tokens import owner_access_token


def build_vulnerability_mail_message(domain, username, vulnerability, request, hide_token=False):
    """ returns the email message for sending to domain owner """
    uidb64 = urlsafe_base64_encode(force_bytes(vulnerability.pk)).decode('utf-8')
    token = owner_access_token.make_token(vulnerability)
    link = request.build_absolute_uri(
        reverse('owner-bug-details', kwargs={'pk': vulnerability.pk, 'uidb64': uidb64, 'token': token}))
    if hide_token:
        link = "LINK HIDDEN"
    message = render_to_string(
        "messages/bug_bounty_mail.html", {
            'vulnerability': vulnerability, 'link': link, 'request': request})
    return message


def send_vulnerability_mail(request, vulnerability, publish_vulnerability):
    """ sending a notification mail to domain owner with link to vulnerability """
    if publish_vulnerability:
        recipient_list = [vulnerability.owner_contact_email]

        if not vulnerability.disclose_deadline:
            vulnerability.disclose_deadline = datetime.datetime.now() + datetime.timedelta(
                days=BUG_BOUNTY_DAYS_TILL_DISCLOSURE)
        message = build_vulnerability_mail_message(
            vulnerability.domain.domain, request.user.username,
            vulnerability, request)
        subject = render_to_string("messages/bug_bounty_mail_subject.html", {'vulnerability': vulnerability})
        send_mail(subject, message, None, recipient_list)
        vulnerability.send_to_owner = True
        if not vulnerability.send_to_owner_at:
            vulnerability.send_to_owner_at = datetime.datetime.now()
        vulnerability.save()
        if vulnerability.send_email_copy_to_user:
            message = build_vulnerability_mail_message(
                vulnerability.domain.domain, request.user.username,
                vulnerability, request, hide_token=True)
            send_mail(subject + " (copy)", message, request.user.email,
                [request.user.email])
