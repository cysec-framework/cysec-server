""" module to test bug bounty api """
from django.urls import reverse
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase
from bug_bounty_mode import models


class BugBountyAPITests(APITestCase):
    """ Bug bounty API TestCase """

    def setUp(self):
        self.user = User.objects.create(username="testuser", password="password")

    def test_create_vulnerability(self):
        """ test if we can create vulnerabilities """
        url = reverse('api:bug-bounty-api:create-bug')
        vuln_type = models.VulnerabilityType.objects.create(name="Cross-Site-Scripting (reflected)")
        domain = models.BugBountyDomain.objects.create(domain="http://example.com")
        data = {
            'domain': domain.pk,
            'path': '/',
            'vuln_type': vuln_type.pk,
            'parameter': 'search',
            'owner_contact_email': 'info@domain.com',
            'poc': 'some proof of concept'
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(models.BugBountyVulnerability.objects.count(), 1)
        self.assertEqual(models.BugBountyVulnerability.objects.filter(user=self.user).count(), 1)

    def test_create_domain(self):
        """ test if we can create a new domain """
        url = reverse('api:bug-bounty-api:create-domain')
        data = {'domain': 'http://example.com'}
        self.client.force_login(user=self.user)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(models.BugBountyDomain.objects.count(), 1)

    def test_create_vulnerability_type(self):
        """ ensure we can create a new vulnerability type """
        url = reverse('api:bug-bounty-api:create-vulnerability-type')
        data = {'name': 'test', 'description': 'asdf', 'consequences': 'asdf', 'how_to_fix': 'qwer'}
        self.client.force_login(user=self.user)
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(models.VulnerabilityType.objects.count(), 1)

    def test_receive_vulnerabilities(self):
        """ are we able to receive our bug bount vulnerabilities """
        url = reverse('api:bug-bounty-api:list-bugs')
        user = User.objects.get(username='testuser')
        self.client.force_authenticate(user=user)
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_login_required_vuln_creation(self):
        """ test if we could create vulnerability without authentication """
        url = reverse('api:bug-bounty-api:create-bug')
        response = self.client.post(url, data={}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_login_required_list_vulns(self):
        """ test if we could list vulnerabilities without authentication """
        url = reverse('api:bug-bounty-api:list-bugs')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
