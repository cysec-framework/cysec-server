""" module for testing bug bounty views """
from django.test import TestCase
from django.contrib.auth.models import User
from django.urls import reverse
from django.core import mail
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from bug_bounty_mode import models
from bug_bounty_mode.utils.auth.tokens import owner_access_token
from bug_bounty_mode.models import BugBountyVulnerability, BugBountyDomain, Comment, VulnerabilityType
from django_dynamic_fixture import G

class BugBountyViewsTestCase(TestCase):

    def setUp(self):
        self.user = G(User)
        self.vuln_type = G(models.VulnerabilityType)
        self.domain = G(BugBountyDomain)
        self.bug = G(BugBountyVulnerability, send_to_owner=True, disclosed=False, user=self.user, vuln_type=self.vuln_type)

    def test_list_users_draft_view(self):
        """ test if we could list our drafts """
        url = reverse('bug_bounty_list_users_drafts')
        self.client.force_login(user=self.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_vulnerability(self):
        """ test if we can create a new vulnerability """
        url = reverse('bug_bounty_submit')
        self.client.force_login(user=self.user)
        payload = {
            'domain_name': 'http://example.com',
            'vuln_type': self.vuln_type.pk,
            'path': '/', 'parameter': 'id', 'owner_contact_email': 'info@mail.com',
            'publish_vulnerability': True,
            'poc': 'some proof-of-concept'
        }
        response = self.client.post(url, payload, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(models.BugBountyVulnerability.objects.count(), 2)
        self.assertEqual(len(mail.outbox), 1)

    def test_invalid_owner_access_token(self):
        """ test if we get 404 not found, if we dont have access token """
        url = reverse('owner-bug-details', kwargs={'token': 'random', 'uidb64': 'invalid', 'pk': self.bug.pk})
        response = self.client.get(url)
        self.assertEquals(response.status_code, 404)

    def test_valid_owner_access_token(self):
        """ test if we get bug details, if we use valid access token of owner """
        uidb64 = urlsafe_base64_encode(force_bytes(self.bug.pk)).decode('utf-8')
        token = owner_access_token.make_token(self.bug)
        url = reverse('owner-bug-details', kwargs={'token': token, 'uidb64': uidb64, 'pk': self.bug.pk})
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def test_resend_email(self):
        """ test resending of email """
        self.client.force_login(user=self.user)
        url = reverse('bug_bounty_resend_email_view', kwargs={'pk': self.bug.pk})
        response = self.client.post(url, follow=True)
        self.assertEqual(len(mail.outbox), 1)

    def test_unauth_new_comment(self):
        """ checking if we can submit a comment without permissions """
        user2 = G(User)
        url = reverse('bug_bounty_comment_vuln', kwargs={'pk': self.bug.pk})
        self.client.force_login(user=user2)
        payload = {'comment': 'Test Comment'}
        response = self.client.post(url, payload)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(Comment.objects.count(), 0)

    def test_auth_new_comment(self):
        """ checking if logged in user can submit comment """
        self.client.force_login(user=self.user)
        payload = {'comment': 'Test Comment'}
        url = reverse('bug_bounty_comment_vuln', kwargs={'pk': self.bug.pk})
        response = self.client.post(url, payload)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Comment.objects.count(), 1)

    def test_create_vuln_type(self):
        """ ensure we can create new vuln type """
        url = reverse('create_new_vuln_type')
        payload = {
            'name': "RFI",
            'description': 'some description',
            'how_to_fix': 'some info',
            'consequences': 'lorem ipsum'
        }
        self.client.force_login(user=self.user)
        response = self.client.post(url, payload)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(VulnerabilityType.objects.count(), 2)

    def test_edit_vuln_type(self):
        """ ensure we can edit vuln type """
        url = reverse('edit_vuln_type', kwargs={'slug': self.vuln_type.slug})
        payload = {
            'name': 'anothername',
            'how_to_fix': 'some info',
            'description': 'some description',
            'consequences': 'lorem ipsum'
        }
        self.client.force_login(user=self.user)
        response = self.client.post(url, payload)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(VulnerabilityType.objects.filter(name="anothername").count(), 1)

    def test_single_vuln_type(self):
        """ ensure we can view vuln type info """
        url = reverse('single_vuln_type', kwargs={'slug': self.vuln_type.slug})
        self.client.force_login(user=self.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_all_vuln_types(self):
        """ ensure we can view all vulntypes """
        url = reverse('list_vuln_types')
        self.client.force_login(user=self.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)