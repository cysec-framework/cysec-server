""" settings for Bug Bounty Mode Application """
from django.conf import settings

BUG_BOUNTY_DAYS_TILL_DISCLOSURE = getattr(
    settings, 'BUG_BOUNTY_DAYS_TILL_DISCLOSURE', 30)


MASTODON_ENABLED = getattr(settings, "MASTODON_ENABLED", False)
MASTODON_URL = getattr(settings, "MASTODON_URL", None)
MASTODON_EMAIL = getattr(settings, "MASTODON_EMAIL", None)
MASTODON_PASSWORD = getattr(settings, "MASTODON_PASSWORD", None)

# use placeholder here
MASTODON_TOOT_MESSAGE = getattr(settings, "MASTODON_TOOT_MESSAGE", """
One of our bug hunters found a {{VULNERABILITY}}. Vendor contacted! More information: {{LINK}}
#cysecframework
""")
