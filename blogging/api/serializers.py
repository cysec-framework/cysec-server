from rest_framework import serializers
from blogging.models import Blog, BlogPost


class BlogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Blog
        fields = ('slug', 'title', 'user')
        read_only_fields = ('slug', 'user')

    def create(self, validated_data):
        blog = Blog.objects.create(
            user=self.context.get('request').user,
            **validated_data)
        return blog


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = BlogPost
        fields = ('slug', 'blog', 'title', 'post')
        read_only_fields = ('slug', 'blog')

    def create(self, validated_data):
        blog = self.context.get('request').user.blog
        post = BlogPost.objects.create(blog=blog, **validated_data)
        return post
