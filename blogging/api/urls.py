from rest_framework.routers import DefaultRouter
from blogging.api import views

app_name = 'blogging-api'

router = DefaultRouter()
router.register(r'', views.BlogViewSet, basename='blog')
#router.registry(r'posts', views.PostViewSet, basename='post')
urlpatterns = router.urls
