from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from blogging.api import serializers
from blogging import models


class BlogViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.BlogSerializer
    queryset = models.Blog.objects.all()


class PostViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.PostSerializer
    queryset = models.BlogPost.objects.all()

