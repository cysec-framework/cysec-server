from django.contrib import admin

from blogging.models import Blog, BlogPost
# Register your models here.

admin.site.register(Blog)
admin.site.register(BlogPost)
