""" module to test bug bounty api """
from django.urls import reverse
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase
from blogging.models import Blog, BlogPost

class BloggingAPITests(APITestCase):
    """ Blogging API TestCase """

    def setUp(self):
        User.objects.create(username="testuser", password="password")


    def test_create_blog(self):
        """ test if we can create new blog """
        url = reverse('api:blogging-api:blog-list')
        payload = {'title': 'Test Blog'}
        user = User.objects.get(username='testuser')
        self.client.force_authenticate(user=user)
        response = self.client.post(url, payload, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Blog.objects.count(), 1)
        self.assertEqual(Blog.objects.filter(user=user).count(), 1)


    #def test_create_post(self):
    #    """ test if we can create new post """
    #    url = reverse('api:create_post')
    #    payload = {'title': 'Test Post', 'post': 'lorem ipsum'}
    #    user = User.objects.get(username='testuser')
    #    blog = Blog.objects.create(user=user, title="Test Blog")
    #    self.client.force_authenticate(user=user)
    #    response = self.client.post(url, payload, format='json')
    #    self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #    self.assertEqual(BlogPost.objects.count(), 1)
    #    self.assertEqual(BlogPost.objects.filter(blog=blog).count(), 1)
