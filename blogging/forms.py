from django import forms
from blogging.models import Blog, BlogPost

class CreateBlogForm(forms.ModelForm):
    class Meta:
        model = Blog
        fields = ('title', )

    def __init__(self, *args, **kwargs):
        super(CreateBlogForm, self).__init__(*args, **kwargs)
        for _key, field in self.fields.items():
            field.widget.attrs.update({'class': 'form-control'})


class CreatePostForm(forms.ModelForm):
    class Meta:
        model = BlogPost
        fields = ('title', 'post', )

    def __init__(self, *args, **kwargs):
        super(CreatePostForm, self).__init__(*args, **kwargs)
        for _key, field in self.fields.items():
            field.widget.attrs.update({'class': 'form-control'})