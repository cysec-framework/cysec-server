from django.urls import path
from blogging import views

app_name = "blogging"

urlpatterns = [

    # create a new blog for this user
    path('new', views.CreateBlog.as_view(), name="create_blog"),

    # displays user blog
    path('user_blogs/<str:blog_slug>', views.BlogIndex.as_view(), name="index"),

    # delete user blog
    path('user_blogs/<str:blog_slug>/delete', views.DeleteBlog.as_view(), name="delete_blog"),

    # display a single blog post
    path(
        'user_blogs/<str:blog_slug>/posts/<str:post_slug>',
        views.SinglePost.as_view(), name="single_post"),

    # create a new post for given blog
    path(
        'posts/new',
        views.CreatePost.as_view(), name="create_post"),

]