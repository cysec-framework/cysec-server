from django.urls import path
from todoz import views

app_name = "todoz"

urlpatterns = [
    path('', views.ListTasks.as_view(), name="all_tasks"),

    path('new', views.CreateTask.as_view(), name="create_task"),

    path('<int:pk>', views.SingleTask.as_view(), name="single_task"),

    path('<int:pk>/edit', views.EditTask.as_view(), name="edit_task"),

    path('<int:pk>/done', views.MarkTaskDoneOrUndone.as_view(), name="mark_task_done"),

    path('<int:pk>/delete', views.DeleteTask.as_view(), name="delete_task"),

    path('<int:pk>/comments/new', views.CreateComment.as_view(), name="create_comment"),
]