from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from markdownx.models import MarkdownxField
from markdownx.utils import markdownify
import bleach
# Create your models here.


class Task(models.Model):
    """ Task database model """
    title = models.CharField(max_length=32)
    description = MarkdownxField(blank=True, null=True)
    assigned_to = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, related_name="assigned_to", blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    creator = models.ForeignKey(User, on_delete=models.CASCADE, related_name="creator")
    done = models.BooleanField(default=False)
    project = models.ForeignKey('pentesting.Project', on_delete=models.CASCADE)

    def __str__(self):
        return self.title + " (%s)" % self.project.name

    def get_absolute_url(self):
        return reverse('todoz:single_task', kwargs={'pk': self.pk})

    @property
    def formatted_markdown(self):
        if self.description:
            self.description = bleach.clean(self.description)
            return markdownify(self.description)
        return None

    class Meta:
        ordering = ('-created_at', )


class Comment(models.Model):
    """ comment database model """
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    text = MarkdownxField() # Not output text to browser use formatted_markdown


    def __str__(self):
        return self.text + "(%s)" % self.task


    @property
    def formatted_markdown(self):
        self.text = bleach.clean(self.text)
        return markdownify(self.text)


class Label(models.Model):
    pass
