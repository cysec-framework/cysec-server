from django.apps import AppConfig


class TodozConfig(AppConfig):
    name = 'todoz'
