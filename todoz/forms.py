from django import forms
from django.contrib.auth.models import User
from todoz.models import Task, Comment


class CreateTaskForm(forms.ModelForm):
    """ form for create a new task """
    class Meta:
        model = Task
        fields = ('title', 'description', 'assigned_to')

    def __init__(self, project, *args, **kwargs):
        super(CreateTaskForm, self).__init__(*args, **kwargs)
        self.fields['assigned_to'].queryset = User.objects.filter(project=project)


class CreateCommentForm(forms.ModelForm):
    """ form for comment creation """
    class Meta:
        model = Comment
        fields = ('text',)
