""" module for testing todoz views """
from django.test import TestCase
from django.contrib.auth.models import User
from django.urls import reverse
from pentesting.models import Project
from todoz.models import Task, Comment

class TodozViewsTestCase(object):

    def setUp(self):
        self.user = User.objects.create(username="testuser")
        self.user.set_password("password")
        self.user.save()
        self.project = Project.objects.create(name="Test Project", user=self.user)
        self.user.profile.projects.add(self.project)
        self.user.profile.save()
        self.client.login(username="testuser", password="password")
        session = self.client.session
        session['active_project'] = str(self.project.pk)
        session.save()


    def test_list_tasks(self):
        """ ensure we can list tasks """
        url = reverse('todoz:all_tasks')
        response = self.client.get(url, follow=True)
        self.assertEqual(response.status_code, 200)



    def test_create_tasks(self):
        """ ensure we can create tasks """
        url = reverse('todoz:create_task')
        payload = {
            'title': 'Test Task',
            'description': 'Some description'
        }
        response = self.client.post(url, data=payload)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Task.objects.count(), 1)


    def test_mark_task_done(self):
        """ ensure we can open/close task """
        task = Task.objects.create(creator=self.user, project=self.project, title="test")
        url = reverse('todoz:mark_task_done', kwargs={'pk': task.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Task.objects.filter(done=True).count(), 1)



    def test_single_task(self):
        """ ensure we can view a single task """
        task = Task.objects.create(creator=self.user, project=self.project, title="test")
        url = reverse('todoz:single_task', kwargs={'pk': task.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


    def test_delete_task(self):
        """ ensure we can delete task """
        task = Task.objects.create(creator=self.user, project=self.project, title="test")
        url = reverse('todoz:delete_task', kwargs={'pk': task.pk})
        response = self.client.post(url)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Task.objects.count(), 0)


    def test_edit_task(self):
        """ ensure we can edit task """
        task = Task.objects.create(creator=self.user, project=self.project, title="test")
        url = reverse('todoz:edit_task', kwargs={'pk': task.pk})
        payload = {'title': 'test2'}
        response = self.client.post(url, payload)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(Task.objects.filter(title="test2").exists())


    def test_create_comment(self):
        """ ensure we can create a comment """
        task = Task.objects.create(creator=self.user, project=self.project, title="test")
        url = reverse('todoz:create_comment', kwargs={'pk': task.pk})
        payload = {'text': 'Some test comment'}
        response = self.client.post(url, payload)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Comment.objects.count(), 1)
