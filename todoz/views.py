""" todoz views """
from django.views import generic
from django.shortcuts import get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.db.models import Count
from todoz.models import Task
from todoz import forms
from todoz.utils.mentions import notify_mentioned_users
# Create your views here.


class ListTasks(LoginRequiredMixin, generic.ListView):
    """ list all tasks for current project """
    template_name = "todoz/all_tasks.html"
    context_object_name = "open_tasks"
    paginate_by = 20

    def get_queryset(self):
        return Task.objects.filter(
            project=self.request.project, done=False).annotate(count=Count('pk'))

    def get_context_data(self, **kwargs):
        context = super(ListTasks, self).get_context_data(**kwargs)
        context['closed_tasks'] = Task.objects.filter(
            project=self.request.project, done=True).annotate(count=Count('pk'))
        context['assigned_tasks'] = Task.objects.filter(
            project=self.request.project,
            done=False, assigned_to=self.request.user).annotate(count=Count('pk'))
        return context


class CreateTask(LoginRequiredMixin, generic.CreateView):
    """ create new task view """
    template_name = "todoz/create_task.html"
    form_class = forms.CreateTaskForm

    def form_valid(self, form):
        instance = form.instance
        instance.creator = self.request.user
        instance.project = self.request.project
        instance.save()
        notify_mentioned_users(instance)
        return super(CreateTask, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(CreateTask, self).get_form_kwargs()
        kwargs['project'] = self.request.project
        return kwargs


class MarkTaskDoneOrUndone(LoginRequiredMixin, generic.RedirectView):
    """ mark a task as done """
    url = reverse_lazy('todoz:all_tasks')

    def get(self, request, *args, **kwargs):
        task = get_object_or_404(Task, pk=self.kwargs.get('pk'), project=self.request.project)
        if task.done:
            task.done = False
        else:
            task.done = True
        task.save()
        return super(MarkTaskDoneOrUndone, self).get(request, *args, **kwargs)


class SingleTask(LoginRequiredMixin, generic.DetailView):
    """ return details of a single task, if it is a task from our project """
    context_object_name = "task"
    template_name = "todoz/single_task.html"

    def get_queryset(self):
        return Task.objects.filter(
            project=self.request.project, pk=self.kwargs.get('pk'))

    def get_context_data(self, **kwargs):
        context = super(SingleTask, self).get_context_data(**kwargs)
        context['new_comment_form'] = forms.CreateCommentForm
        return context


class DeleteTask(LoginRequiredMixin, generic.DeleteView):
    """ delete a task if we are owner """
    success_url = reverse_lazy('todoz:all_tasks')
    context_object_name = "task"

    def get_queryset(self):
        return Task.objects.filter(
            pk=self.kwargs.get('pk'), project=self.request.project,
            creator=self.request.user)


class EditTask(LoginRequiredMixin, generic.UpdateView):
    """ edit a task, if we are owner """
    context_object_name = "task"
    form_class = forms.CreateTaskForm
    template_name = "todoz/edit_task.html"

    def get_queryset(self):
        return Task.objects.filter(
            pk=self.kwargs.get('pk'), project=self.request.project,
            creator=self.request.user)

    def get_form_kwargs(self):
        kwargs = super(EditTask, self).get_form_kwargs()
        kwargs['project'] = self.request.project
        return kwargs

    def form_valid(self, form):
        instance = form.instance
        form.save()
        notify_mentioned_users(instance)
        return super(EditTask, self).form_valid(form)


class CreateComment(LoginRequiredMixin, generic.CreateView):
    """ create new comment for task """
    http_method_names = ['post']
    form_class = forms.CreateCommentForm

    def get_success_url(self):
        task = get_object_or_404(Task, pk=self.kwargs.get('pk'), project=self.request.project)
        return reverse_lazy('todoz:single_task', kwargs={'pk': task.pk})

    def form_valid(self, form):
        task = get_object_or_404(Task, pk=self.kwargs.get('pk'), project=self.request.project)
        instance = form.instance
        instance.task = task
        instance.user = self.request.user
        instance.save()
        notify_mentioned_users(instance)
        return super(CreateComment, self).form_valid(form)
