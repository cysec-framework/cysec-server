from django.views import generic
from django.conf import settings

# Create your views here.


class IndexView(generic.TemplateView):
    template_name = "public_pages/index.html"

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['carousel_images'] = settings.PUBLIC_CAROUSEL_IMAGES
        return context