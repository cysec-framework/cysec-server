from django.core.exceptions import ImproperlyConfigured
from activity_feed import models


class ActivityFeedMixin(object):
    activity_message = None
    activity_title = None

    def get_activity_mesage(self):
        return self.activity_message

    def get_activity_title(self):
        return self.activity_title

    def form_valid(self, form):
        message = self.get_activity_mesage()
        title = self.get_activity_title()
        if not message or not title:
            raise ImproperlyConfigured(
                '%(cls)s is missing a message or title. define '
                "%(cls)s.message, %(cls)s.title or override "
                "%(cls)s.get_activity_message() or %(cls)s.get_activity_title " % {
                    'cls': self.__class__.__name__
                })
        models.Activity.objects.create(user=self.request.user, title=title, message=message)
        return super(ActivityFeedMixin, self).form_valid(form)


class PentestingActivityFeedMixin(ActivityFeedMixin):
    def form_valid(self, form):
        title = self.get_activity_title()
        message = self.get_activity_mesage()
        if not message or not title:
            raise ImproperlyConfigured(
                '%(cls)s is missing a message or title. define '
                "%(cls)s.message, %(cls)s.title or override "
                "%(cls)s.get_activity_message() or %(cls)s.get_activity_title " % {
                    'cls': self.__class__.__name__
                })
        models.PentestingActivity.objects.create(
            user=self.request.user, title=title, message=message, project=self.request.project)
        return super(PentestingActivityFeedMixin, self).form_valid(form)