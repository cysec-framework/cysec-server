from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from activity_feed import models
# Create your views here.


class AllProjectActivities(LoginRequiredMixin, generic.ListView):
    paginate_by = 10
    context_object_name = 'activities'
    template_name = 'activity_feed/project_activities.html'

    def get_queryset(self):
        return models.PentestingActivity.objects.filter(project=self.request.project)