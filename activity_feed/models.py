from django.db import models
from django.contrib.auth.models import User
from pentesting.models import Project
from bug_bounty_mode.models import BugBountyVulnerability
# Create your models here.


class Activity(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='activities')
    created_at = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=32)
    message = models.CharField(max_length=256)


class PentestingActivity(Activity):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)


class BugBountyActivity(Activity):
    bug = models.ForeignKey(BugBountyVulnerability, on_delete=models.CASCADE)