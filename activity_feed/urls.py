from django.urls import path
from activity_feed import views

app_name = 'activity-feed'

urlpatterns = [
    path('', views.AllProjectActivities.as_view(), name='all-project-activities'),
]