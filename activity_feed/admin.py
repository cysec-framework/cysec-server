from django.contrib import admin
from activity_feed import models
# Register your models here.

admin.site.register(models.Activity)
admin.site.register(models.PentestingActivity)
admin.site.register(models.BugBountyActivity)
