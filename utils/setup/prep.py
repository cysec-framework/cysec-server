from django.utils.crypto import get_random_string


def gen_secret_key(key_file):
    """ generates a new secret key """
    chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
    key = get_random_string(200, chars)
    with open(key_file, 'w') as my_file:
        my_file.write("SECRET_KEY = '%s'" % key)
