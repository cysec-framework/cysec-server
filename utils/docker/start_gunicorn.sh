#!/bin/sh

# start crond
echo Starting cron-daemon
crond -b

# collect static files
echo collecting static files
python3 manage.py collectstatic --noinput

# migrating database
echo Migrating database
python3 manage.py migrate

# Start Gunicorn processes
echo Starting Gunicorn.
gunicorn cysec_server.wsgi:application \
    --bind 0.0.0.0:8000 \
    --workers 3
