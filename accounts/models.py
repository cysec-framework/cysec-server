from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save


def get_user_image_folder(instance, filename):
    return "%s/avatars/%s" %(instance.user.pk, filename)


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    bio = models.TextField(blank=True)
    email_confirmed = models.BooleanField(default=False)
    enable_public_mailbox = models.BooleanField(default=False)
    public_key = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.user.username


class AcceptedDonation(models.Model):
    key = models.CharField(max_length=32)
    value = models.CharField(max_length=255)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Accepted Donations"
        verbose_name_plural = "Accepted Donations"

    def __str__(self):
        return "%s:%s" % (self.user, self.key)


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)


post_save.connect(create_user_profile, sender=User)
