from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes
from django.utils import timezone
from django.utils.http import urlsafe_base64_encode
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.contrib.auth import views
from django.urls import reverse_lazy
from django.conf import settings
from django.db.models import Count
from django.db.models.functions import ExtractMonth
from rest_framework.authtoken.models import Token
from accounts.tokens import verify_account_token_generator
from .forms import ChangePasswordForm
from .forms import ChangeEMailAddressForm
from .app_settings import REGISTRATION_ENABLED, SIGNUP_ACTIVATION_EMAIL_SUBJECT
from accounts import forms
from pentesting.models import Project
import calendar


class AccountLoginView(views.LoginView):
    """ Login View for user account """
    template_name = "accounts/login.html"
    redirect_authenticated_user = True

    def get_success_url(self):
        return reverse_lazy('accounts:user_profile', kwargs={'user_id':self.request.user.pk})

    def form_valid(self, form):
        user = form.get_user()
        projects = Project.objects.filter(userproject__user=user)
        if self.request.session.get('active_project'):
            del self.request.session['active_project']
        project = projects.order_by('-start_date').first()
        if project:
            self.request.session['active_project'] = str(project.pk)
        return super(AccountLoginView, self).form_valid(form)


class SignUpView(generic.FormView):
    """ signup a new account on a server, if registration is enabled """
    form_class = forms.SignUpForm
    template_name = "accounts/signup.html"
    extra_context = {'registration_enabled': REGISTRATION_ENABLED}
    success_url = reverse_lazy('accounts:login_view')

    def form_valid(self, form):
        if REGISTRATION_ENABLED:
            instance = form.instance
            instance.is_active = False
            instance.save()
            current_site = get_current_site(self.request)
            subject = SIGNUP_ACTIVATION_EMAIL_SUBJECT
            message = render_to_string('account_activation_email.html', {
                'user': instance,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(instance.pk)).decode('utf-8'),
                'token': verify_account_token_generator.make_token(instance),
            })
            send_mail(
                subject, message,
                settings.DEFAULT_FROM_EMAIL, [instance.email], fail_silently=False)
            messages.add_message(
                self.request, messages.SUCCESS, 'Activation mail send to your email address!')
        else:
            messages.add_message(self.request, messages.ERROR, 'Registration disabled!')
        return super(SignUpView, self).form_valid(form)


class ActivateAccountView(generic.RedirectView):
    """ activate user account by one-time token """
    url = reverse_lazy('accounts:login_view')

    def get(self, request, *args, **kwargs):
        try:
            uid = force_text(urlsafe_base64_decode(self.kwargs.get('uidb64')))
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        if user is not None and verify_account_token_generator.check_token(user, self.kwargs.get('token')):
            user.is_active = True
            user.profile.email_confirmed = True
            user.profile.save()
            login(request, user)
        else:
            messages.add_message(request, messages.ERROR, 'Could not activate your account')
        return super(ActivateAccountView, self).get(request, *args, **kwargs)


class ValidateChangeEMailAddressView(generic.RedirectView):
    url = reverse_lazy('accounts:login_view')

    def get(self, request, *args, **kwargs):
        try:
            uid = force_text(urlsafe_base64_decode(self.kwargs.get('uidb64')))
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        if user is not None and verify_account_token_generator.check_token(user, self.kwargs.get('token')):
            user.profile.email_confirmed = True
            user.profile.save()
            messages.add_message(request, messages.SUCCESS, 'E-Mail confirmation successful!')
        else:
            messages.add_message(request, messages.ERROR, 'Could not validate your email address')
        return super(ValidateChangeEMailAddressView, self).get(request, *args, **kwargs)


class ChangeEMailAddressView(generic.FormView):
    """ handle email address changes """
    form_class = ChangeEMailAddressForm
    success_url = reverse_lazy('accounts:login_view')
    template_name = "accounts/change_email_address.html"

    def get_initial(self):
        initial = super(ChangeEMailAddressView, self).get_initial()
        initial['email'] = self.request.user.email
        return initial

    def form_valid(self, form):
        user = self.request.user
        user.profile.email_confirmed = False
        user.email = form.cleaned_data['email']
        user.save()
        current_site = get_current_site(self.request)
        subject = "Confirm E-Mail Address"
        message = render_to_string('accounts/emails/confirm_email_change.html', {
            'user': user,
            'domain': current_site.domain,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode('utf-8'),
            'token': verify_account_token_generator.make_token(user),
        })
        send_mail(
            subject, message,
            settings.DEFAULT_FROM_EMAIL, [user.email], fail_silently=False)
        return super(ChangeEMailAddressView, self).form_valid(form)


class ChangePasswordView(LoginRequiredMixin, generic.FormView):
    form_class = ChangePasswordForm
    template_name = "accounts/change_password.html"
    success_url = reverse_lazy("accounts:change_password")

    def form_valid(self, form):
        if form.cleaned_data['password1'] == form.cleaned_data['password2']:
            try:
                validate_password(form.cleaned_data['password1'])
                self.request.user.set_password(form.cleaned_data['password1'])
                self.request.user.save()
                update_session_auth_hash(self.request, self.request.user)
                messages.add_message(
                    self.request, messages.SUCCESS, 'Password changed successfully!')
            except ValidationError as error:
                messages.add_message(self.request, messages.ERROR, error)
        else:
            messages.add_message(self.request, messages.ERROR, "Passwords not matching!")
        return super(ChangePasswordView, self).form_valid(form)


class Index(LoginRequiredMixin, generic.RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        return reverse_lazy('user_profile', kwargs={'user_id':self.request.user.pk})


class ShowUserProfile(generic.TemplateView):
    template_name = 'accounts/user_profile.html'

    def get_context_data(self, **kwargs):
        context = super(ShowUserProfile, self).get_context_data(**kwargs)
        context['user'] = User.objects.get(pk=self.kwargs.get('user_id'))
        return context


class ListVulnsFoundByUser(LoginRequiredMixin, generic.ListView):
    context_object_name = 'user'
    template_name = 'accounts/users_vulns.html'

    def get_queryset(self):
        queryset = User.objects.get(pk=self.kwargs.get('user_id'))
        return queryset


class PasswordResetView(views.PasswordResetView):
    """ reset a users password, if he lost it """
    email_template_name = "accounts/registration/password_reset_email.html"
    from_email = settings.DEFAULT_FROM_EMAIL
    subject_template_name = "accounts/registration/password_reset_subject.txt"


class PasswordResetConfirmView(views.PasswordResetConfirmView):
    template_name = "accounts/registration/password_reset_confirm.html"

    def get_context_data(self, **kwargs):
        context = super(PasswordResetConfirmView, self).get_context_data(**kwargs)
        form = context['form']
        for _key, field in form.fields.items():
            field.widget.attrs.update({'class':'form-control'})
        return context


class RefreshAPIToken(LoginRequiredMixin, generic.RedirectView):
    http_method_names = ['post']
    url = reverse_lazy('accounts:settings')

    def post(self, request, *args, **kwargs):
        if not Token.objects.filter(user=self.request.user).exists():
            Token.objects.create(user=self.request.user)
        Token.objects.get(user=self.request.user).delete()
        Token.objects.create(user=self.request.user)
        return super(RefreshAPIToken, self).post(request, *args, **kwargs)


class Settings(LoginRequiredMixin, generic.TemplateView):
    template_name = "accounts/settings.html"


class Dashboard(LoginRequiredMixin, generic.TemplateView):
    template_name = "accounts/dashboard.html"

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)
        context['fixed_bugs'] = self.request.user.bugbountyvulnerability_set.filter(fixed=True).count()
        context['all_bugs'] = self.request.user.bugbountyvulnerability_set.count()
        context['all_projects'] = Project.objects.filter(userproject__user=self.request.user).count()
        context['upcoming_projects'] = Project.objects.filter(
            due_date__isnull=False, due_date__gte=timezone.now(), userproject__user=self.request.user).order_by('due_date')[:5]
        latest_bugs = self.request.user.bugbountyvulnerability_set.annotate(
            month=ExtractMonth('created_at')).values('month').annotate(count=Count('pk')).values('count', 'month').order_by('month')
        context['latest_bugs_labels'] = []
        context['latest_bugs_data'] = []
        for bug in latest_bugs:
            context['latest_bugs_labels'].append(bug['month'])
            context['latest_bugs_data'].append(bug['count'])
        return context
