from django.urls import path
from accounts.api import views

app_name = "accounts-api"

urlpatterns = [
    path("refresh-api-token", views.RefreshAPIToken.as_view(), name="refresh-api-token"),

    path('enable-public-mailbox', views.UpdateEnablePublicMailbox.as_view(), name="enable-public-mailbox"),

    path('<int:pk>/fetch-public-key', views.UsersMailboxPublicKey.as_view(), name="fetch-public-key"),
]