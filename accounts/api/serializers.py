from rest_framework import serializers
from rest_framework.authtoken.models import Token
from accounts.models import UserProfile


class RefreshAPITokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Token
        fields = ('key', )
        read_only_fields = ('key', )
    
    def update(self, instance, validated_data):
        user = instance.user
        instance.delete()
        return Token.objects.create(user=user)



class UpdateEnablePublicMailboxSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ('enable_public_mailbox', )


class MailboxPublicKeySerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ('public_key', )
        read_only_fields = ('public_key',)