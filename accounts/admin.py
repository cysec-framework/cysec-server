from django.contrib import admin
from .models import UserProfile, AcceptedDonation
# Register your models here.

admin.site.register(UserProfile)
admin.site.register(AcceptedDonation)
