""" app settings for account application """
from django.conf import settings

# Allow external users to register new accounts
REGISTRATION_ENABLED = getattr(settings, "REGISTRATION_ENABLED", False)

# One-Time Tokens are valid for 14 days (default)
PASSWORD_RESET_TIMEOUT_DAYS = getattr(settings, "PASSWORD_RESET_TIMEOUT_DAYS", 14)

# The Subject for the email field
SIGNUP_ACTIVATION_EMAIL_SUBJECT = "Activate your CySec Account"
