import hashlib
import urllib
from django import template
from django.utils.safestring import mark_safe

register = template.Library()

# return only the URL of the gravatar
# TEMPLATE USE:  {{ email|gravatar_url:150 }}
@register.filter
def gravatar_url(email, size=40):
    """ generate gravatar url for this users email address
        since MD5 is broken, we want to prevent sending email
        address of user, directly to server, but instead we use
        a SHA-512 version of the email address. We force default image to be identicon.
    """
    secured_email = hashlib.sha512(email.encode('utf-8').lower()).hexdigest()
    gravatar_hashed = hashlib.md5(secured_email.encode('utf-8').lower()).hexdigest()
    return "https://www.gravatar.com/avatar/%s?%s" % (
        gravatar_hashed, urllib.parse.urlencode({'d':'identicon', 's':str(size), 'f': 'y'}))

# return an image tag with the gravatar
# TEMPLATE USE:  {{ email|gravatar:150 }}
@register.filter
def gravatar(email, size=40):
    url = gravatar_url(email, size)
    return mark_safe(
        '<img class="img-fluid z-depth-1" src="%s" width="%d" height="%d">' % (
            url, size, size))
