from collections import defaultdict
from django import template
from pinax.badges.models import BadgeAward
from pinax.badges.registry import badges


register = template.Library()


@register.filter
def user_badges(user):
    user_awards = defaultdict(list)
    awards = BadgeAward.objects.filter(user=user).values('slug', 'level')
    for award in awards:
        if award['slug'] not in user_awards.keys():
            new_badge = {
                'levels': []
            }
            for index, level in enumerate(badges._registry[award['slug']].levels):
                new_level = {'level': level, 'awarded': False, 'index': index}
                if index == award['level']:
                    new_level['awarded'] = True
                new_badge['levels'].append(new_level)
            user_awards[award['slug']].append(new_badge)
        else:
            levels = user_awards[award['slug']][0].get('levels')
            for level in levels:
                if level['index'] == award['level']:
                    level['awarded'] = True
    return dict(user_awards).items()
