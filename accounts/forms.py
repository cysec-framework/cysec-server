from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from accounts import models


class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', )


class ChangePasswordForm(forms.Form):
    password1 = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        label='Password', max_length=255)
    password2 = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        label='Password confirm', max_length=255)


class ChangeEMailAddressForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('email',)


class EditProfileForm(forms.ModelForm):
    class Meta:
        model = models.UserProfile
        fields = ('bio',)

